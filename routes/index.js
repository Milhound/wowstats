var express = require('express');
var querystring = require('querystring');
var router = express.Router();
var https = require('https');

process.on('uncaughtException', function (err) {
    console.log(err);
}); 

function getFaction(info){
  var alliance;
  switch(info.faction){
      case 0:
          alliance = 'Alliance';
          break;
      case 1:
          alliance = 'Horde';
          break;
      default:
          alliance = 'Unknown';
  }
  return alliance
}

function getRace(info){
  var racename;
  switch(info.race){
    case 1:
        racename = 'Human';
        break;
    case 2:
        racename = 'Orc';
        break;
    case 3:
        racename = 'Dwarf';
        break;
    case 4:
        racename = 'Night Elf';
        break;
    case 5:
        racename = 'Undead';
        break;
    case 6:
        racename = 'Tauren';
        break;
    case 7:
        racename = 'Gnome';
        break;
    case 8:
        racename = 'Troll';
        break;
    case 10:
        racename = 'Blood Elf';
        break;
    case 11:
        racename = 'Draenai';
        break;
    case 22:
        racename = 'Worgen';
        break;
    case 25:
        racename = 'Panda';
        break;
    default:
        racename = 'Unknown';
  }
  return racename
}

function getClass(info){
  var classname;
  switch(info.class){
      case 1:
          classname = 'Warrior';
          break;
      case 2:
          classname = 'Paladin';
          break;
      case 3:
          classname = 'Hunter';
          break;
      case 4:
          classname = 'Rogue';
          break;
      case 5:
          classname = 'Priest';
          break;
      case 6:
          classname = 'Death Knight';
          break;
      case 7:
          classname = 'Shaman';
          break;
      case 8:
          classname = 'Mage';
          break;
      case 9:
          classname = 'Warlock';
          break;
      case 10:
          classname = 'Monk';
          break;
      case 11:
          classname = 'Druid';
          break;
      default:
          classname = 'Unknown';
  }
  return classname
}

router.get('/', (req,res, next) => {
  res.send('Welcome to my website. Please go to /Server-Name/CharacterName for info about your character. Example: https://wowstats.xyz/Draenor/Milhound.');
});

/* GET home page. */
router.get('/:realm/:character/:fields*?', function(req, res, next) {
  var name, fields, information;
  var base_url = 'us.api.battle.net';
  var api = '&locale=en_US&apikey=' + process.env.API_KEY;
  name = querystring.escape(req.params.character);
  realm = querystring.escape(req.params.realm);
  if(req.params.fields){
    field = '?fields=' + (req.params.fields).toLowerCase();
  }else{
    field = '?fields=statistics'
  }
  var path = '/wow/character/' + realm + '/' + name + field + api;

  var options = {
    host: base_url,
    path: path,
    method: 'GET'
  }

  callback = function(response) {
    var json = '';
    response.on('data', function (chunk) {
      json += chunk;
    });
    response.on('end', function () {
      information = JSON.parse(json);
      pageTitle = information.name + ' - Stats';
      console.log(base_url + path);
      var race = getRace(information);
      var alliance = getFaction(information);
      var classname = getClass(information);
      console.log(alliance);
      res.render('index', { title: pageTitle, info: information, race: race, alliance: alliance, classname: classname});
      
    });
  }
  req = https.request(options, callback);
  req.end();
 
});

module.exports = router;
